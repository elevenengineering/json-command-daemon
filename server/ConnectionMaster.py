import socket, os, stat, ssl
from server.Conf import kSocketTypeMap, kSSLClientAuthMap
from server import ClientConnection, Conf

kMasterSocketTimeout = 0.2 #s

class ConnectionMaster():
  def __init__(self, log_queue):
    self.__socket = open_main_socket(
      kSocketTypeMap[Conf.kConf['socket_type']], Conf.kConf['socket_address'])
    self.__context = setup_ssl_context()
    self.__socket.settimeout(kMasterSocketTimeout)
    self.__log_queue = log_queue
    self.__thrd_list = [] #this really should be a singly-linked list :(

  def step(self):
    self.__accept_client()
    self.__thrd_list[:] = [ t for t in self.__thrd_list if t.is_alive ]

  def stop_all(self):
    for thrd in self.__thrd_list:
      thrd.stop()
    for thrd in self.__thrd_list:
      thrd.join()
    self.__thrd_list[:] = []
    try:
      self.__socket.shutdown(socket.SHUT_RDWR)
      self.__socket.close()
    except:
      pass
    finally:
      if self.__socket.family == socket.AF_UNIX:
        os.unlink(Conf.kConf['socket_address'])

  def __log_msg(self, s, peername='unknown'):
    self.__log_queue.put((s, peername, 'none', True))

  # Spawns an independent thread to handle a client session when a connection is
  # accepted on the main server socket
  def __accept_client(self):
    try:
      sock = self.__socket.accept()[0]
      peer = sock.getpeername()
      self.__log_msg('Inbound connection from', peer)
      if not self.__context is None:
        sock = self.__context.wrap_socket(sock, server_side=True)
    except (socket.timeout, InterruptedError):
      return
    except Exception as e:
      self.__log_msg(' '.join([str(e), 'with peer']), peer)
      try:
        sock.shutdown(socket.SHUT_RDWR)
        sock.close()
      except:
        pass
      return
    conn = ClientConnection.ClientConnection(sock, self.__log_queue)
    conn.start()
    self.__thrd_list.append(conn)


# Opens/binds the main socket that listens for client connections.
# Returns the socket obj itself.
def open_main_socket(sock_type, sock_addr):
  if sock_type == socket.AF_UNIX:
    sock_addr = os.path.abspath(sock_addr)
    if os.path.exists(sock_addr):
      raise RuntimeError('%s already exists, unable to create socket file!'
        %sock_addr)
  else:
    sock_addr = tuple(sock_addr)
  s = socket.socket(sock_type, socket.SOCK_STREAM)
  try:
    s.bind(sock_addr)
    if s.family == socket.AF_UNIX:
      os.chmod(sock_addr, stat.S_IRUSR|stat.S_IWUSR|stat.S_IRGRP|stat.S_IWGRP|
        stat.S_IROTH|stat.S_IWOTH) #FIXME: this should DEFINITELY be configurable!
    s.listen(1)
  except:
    s.shutdown(socket.SHUT_RDWR)
    s.close()
    if sock_type == socket.AF_UNIX:
      os.unlink(sock_addr)
    raise
  return s

def setup_ssl_context():
  if not Conf.kConf['ssl_enabled']:
    return None
  context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
  context.verify_mode = kSSLClientAuthMap[Conf.kConf['ssl_client_auth']]
  context.options = \
    ssl.OP_CIPHER_SERVER_PREFERENCE|ssl.OP_SINGLE_DH_USE|ssl.OP_SINGLE_ECDH_USE
  context.verify_flags = ssl.VERIFY_X509_STRICT
  context.load_cert_chain(
    Conf.kConf['ssl_server_cert'], Conf.kConf['ssl_server_key'])
  context.load_verify_locations(Conf.kConf['ssl_ca_cert'])
  return context
