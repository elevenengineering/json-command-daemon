import time
class Timer():
  def __init__(self, duration):
    self.__duration = duration
    self.__end_time = 0

  def Reset(self):
    self.__end_time = time.time() + self.__duration

  def Elapsed(self):
    return (time.time() - self.__end_time) > 0
