import enum, threading
from common import interpret_json

kMsgDelimiter = bytes('\a\f\n', 'ascii')

class MsgType(enum.IntEnum):
  Invalid = 0
  Text = 1
  Raw = 2
  Command = 3
  Response = 4
  CloseConnection = 5
  Ack = 6
  FileRequest = 7

  def LegalFields(self):
    kLegalMsgTypeFields = {
      MsgType.Text: ['body'],
      MsgType.Raw: ['body', 'name'],
      MsgType.Command: ['name', 'subs_list'],
      MsgType.Response: ['code'],
      MsgType.FileRequest: ['name']
    }
    return kLegalMsgTypeFields.get(self, [])

class ResponseCode(enum.IntEnum):
  MsgInvalid = 1
  MsgIgnored = 2
  CmdDone = 3
  CmdFail = 4
  CmdInvalid = 5

class Message(dict):
  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)

  def IsValid(self):
    try:
      # raises a ValueError if not a valid MsgType, or KeyError if 'type' is missing
      mtype = MsgType(self['type'])
      fields = mtype.LegalFields()
      if len(self) != len(fields)+1: # 1 extra field for 'type'
        return False
      for field in fields:
        self[field] #throws KeyError if the particular field is missing
      if mtype == MsgType.Response:
        ResponseCode(self['code'])
    except:
      return False
    return True

  def ToRaw(self):
    return interpret_json.var_to_raw(self)


class RawMsgBuffer(bytearray):
  def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)

  # Returns None if a complete message is not present in the buffer;
  # else, extracts the first message in the buffer
  def extract_msg(self):
    end = self.find(kMsgDelimiter)
    if end != -1:
      try:
        msg = Message(**interpret_json.raw_to_var(
          memoryview(self[0:end]).tobytes()))
      except:
        msg = Message({'type': MsgType.Invalid})
      del self[:end+len(kMsgDelimiter)]
      return msg
    return None

  def append_msg(self, msg):
    self.extend(msg.ToRaw())
    self.extend(kMsgDelimiter)

  def flush(self):
    del self[:]
