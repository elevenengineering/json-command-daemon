================================================================================
Server Configuration File
================================================================================

This is a JSON-formatted file that the server reads in to determine how to
configure itself, which commands to accept from clients, and how to execute
these commands.

The top-level is a JSON object (surrounded by '{}', maps to a Python dictionary)
with the following fields:

"socket_type": "unix", "inet", or "inet6"
"socket_address": For Unix sockets, path to the socket file. Otherwise,
the 2-tuple corresponding to the interface address and port on which to bind.
These determine which type of socket the server should create to listen on. If
"unix" is specified, the socket file is created at the value of "socket_addr";
else, it listens on the interface and port specified in "socket_addr".


"log_file": "/path/to/filename"
The log file where request metadata will be recorded. If null, no logging
occurs.


"client_timeout": positive integer
The number of seconds that a client is allowed to remain quiet or send invalid
messages for before the server considers the connection timed out.


"max_msg_len": positive integer
The maximum number of bytes that a client may send for any single message. If
exceeded, the server will forcibly terminate the connection.


"client_commands": { client_command0, client_command1, ... }
The list of valid commands that clients may instruct the server to perform.

Each client_command is a JSON object structured in the following way:
"name": {
  "exec_list": [ exec_arr0, exec_arr1, ... ],
  "default_list": { substitute_pair0, substitute_pair1, ... },
  "substitution_list": [ substitute_name0, substitute_name1, ... ],
  "dir": "/path/to/dir",
  "fatal_error": boolean,
  "client_output": boolean
}

An exec_list is an array containing arrays of system executables or
pre-defined json-cd commands, along with the arguments which should be passed
to them, executed in the order in which they appear in the exec_list. Namely:

  exec_arr: [ "executable/command name", "ARG0", "ARG1", ... ]
  Arguments may be arbitrary text (devoid of spaces) which the server can then
  replace with more meaningful arguments through the default_list and
  substitution_list.


A default_list is a JSON object containing name-value pairs. The name
specifies the raw text to be substituted within all of the exec_arrs in the
exec_list, and the value is the replacement text. These values are substituted
in before executing a command if a client fails to specify an explicit
substitution for the name, or if the name does not appear in the
substitution_list.


A substitution_list is an array of raw text values (like the default_list
names) for which the client may explicitly specify text to replace them with.
To be used cautiously, as the client may supply arbitrary text. Any names
which appear in the substitution_list which do not appear in the default_list
are considered required arguments and the command will not execute if a client
fails to provide values for any of them.


"substitution_remaps": {
  "name" : {
    "hashed": bool,
    "hash_algorithm": string,
    "salt": string,
    "rounds": unsigned integer,
    "if_missing": "error", "self", or "default",
    "default": arbitrary value,
    "remap_table" : { name_value_pair0, name_value_pair1, ... }
  },
  ...
}
A JSON object which specifies an argument name (raw text that is substituted in
by a client input), and contains a restricted set of name-pair values which
client inputs are remapped into. Argument names apply for all commands within
the configuration file. This allows, for example, a client to send a
password, and if the password is a valid name in the remap table, it is
translated to an appropriate value to correspond to the client "logging in".
 
  "hashed" is a boolean that specifies whether client inputs for the argument
  name should be hashed prior to looking up their values in the remap_table. If
  true, "hash_algorithm" specfies which hash algorithm to use, "salt" specifies
  the salt to use, and "rounds" is the number of times the hash function is
  executed on the input. Hashing is done using Python's hashlib.pbkdf2_hmac(),
  and its binary output is stored as a hexified string.

  "if_missing" is a string which describes the behaviour of the server if the
  client specifies a value for the argument which is not present in the
  remap_table. If "error", the server will refuse to execute the command; if
  "default", the server will map the user input to the value specfied in
  "default"; or if "self", the server will simply pass the user input to the
  command for substitution without re-mapping it.

  The remap_table is a JSON object of key-value pairs; each key corresponds to
  the input value (or its correct hash if "hashed" is true), and the value
  corresponds to the substitution text which should take its place.


================================================================================
Protocol Message Structure
================================================================================

A JSON message sent between client and server is typically structured in such a
way:
{
  "type": enumeration,
  (possible additional data field name): ascii/base64-encoding string,
  ...
}
The message type determines what additional fields are mandatory for the message
to be considered valid. These are described in common/protocol.py.


================================================================================
Client-Server Interaction
================================================================================

Client applications send "cmd" JSON messages with arguments to the server, which
in turn interprets them and executes the pre-defined executables comprising the
command, modified by the arguments. The server sends all of the output from the
executables to the client as "text" JSON messages, and may also request files
from the client (using a "resp" message) and send files to the client (using a
"raw" message). The server will report the status of the command's
failure or success at the end with a "resp" message.
