import queue, threading, abc

class Worker(threading.Thread):
  __metaclass__ = abc.ABCMeta

  def __init__(self):
    super().__init__()

  @abc.abstractmethod
  def run(self):
    return

  @abc.abstractmethod
  def stop(self):
    return


class Consumer(Worker):
  def __init__(self):
    super().__init__()
    self.__in_queue = queue.Queue()

  def put(self, datum, *args, **kwargs):
    self.__in_queue.put(datum, *args, **kwargs)

  def _pop(self, *args, **kwargs):
    return self.__in_queue.get(*args, **kwargs)

  def HasWork(self):
    return not self.__in_queue.empty()


class Producer(Worker):
  def __init__(self):
    super().__init__()
    self.__out_queue = queue.Queue()

  def get(self, *args, **kwargs):
    return self.__out_queue.get(*args, **kwargs)

  def _push(self, datum, *args, **kwargs):
    self.__out_queue.put(datum, *args, **kwargs)
