import hashlib, binascii

def gen_raw_hash(inp, conf):
  return hashlib.pbkdf2_hmac(hash_name=conf['hash_algorithm'],
    salt=bytes(conf['salt'], 'ascii'), iterations=int(conf['rounds']),
    password=inp)

def stringify_hash(raw_hash):
  return binascii.hexlify(raw_hash).decode('ascii')

def destringify_hash(hash_str):
  return binascii.unhexlify(hash_str.encode('ascii'))
