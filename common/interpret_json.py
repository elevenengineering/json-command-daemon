import json

# Reads in the JSON-formatted file located at path and returns a
# dictionary/tuple/array of the parsed variables and their values.
def read_file(path):
  try:
    f_obj = open(path, mode='r')
  except Exception as e:
    print('%s: %s' %(str(e), path))
    raise
  try:
    data = json.load(f_obj)
  except:
    print('Incorrect JSON formatting in file: %s' %path)
    raise
  return data

# Writes out the dictionary/tuple/array 'data' into a JSON-formatted file
# located at path.
def write_file(data, path, indent=2):
  try:
    f_obj = open(path, mode='w')
  except Exception as e:
    print('%s: %s' %(str(e), path))
    raise
  json.dump(data, f_obj, indent=indent, sort_keys=True)
  return

# Interprets the JSON-formatted raw data bytes into a dict/tuple/array.
def raw_to_var(raw):
  return json.loads(str(raw, 'ascii'))

# Interprets the dict/tuple/array into JSON-formatted raw data bytes.
def var_to_raw(var):
  return bytes(json.dumps(var), 'ascii')
