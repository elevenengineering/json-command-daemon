def write_data(data, fd):
  written = 0
  while written < len(data):
    curr_written = fd.write(memoryview(data[written:]).tobytes())
    if curr_written == 0:
      raise RuntimeError('Unable to write file to disk!')
    written += curr_written
