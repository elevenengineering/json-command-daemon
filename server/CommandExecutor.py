import copy, traceback, os, base64, subprocess, re, fcntl, io, queue, signal
from common import easyhash, easyio, Worker
from common.protocol import Message, MsgType, ResponseCode
from server import Conf

class ClientErrorException(Exception):
  pass

class ClientWarningException(Exception):
  pass

class ForceStopException(Exception):
  pass

kQueueTimeout = 0.1 #s
kProcessUpdateInterval = kQueueTimeout

class CommandExecutor(Worker.Consumer, Worker.Producer):
  def __init__(self):
    super().__init__()
    self.working = False
    self.expecting_raw = False
    self.__stop_flag = False

  def run(self):
    while not self.__stop_flag:
      try:
        msg = self._pop(timeout=kQueueTimeout)
      except queue.Empty:
        continue
      self.working = True
      try:
        self.__process_cmd_msg(msg)
      except ForceStopException:
        continue
      except:
        s = io.StringIO()
        traceback.print_exc(file=s)
        self._push(Message(type=MsgType.Text,
          body='\n'.join([ s.getvalue(), ''])))
        s.close()
      finally:
        self.working = False

  def stop(self):
    self.__stop_flag = True

  # Receives a command message, configures it as is appropriate, and attempts to
  # execute it while transmitting feedback to the client
  def __process_cmd_msg(self, msg):
    cmd_name = msg['name']
    try:
      cmd_struct = copy.deepcopy(Conf.kConf['client_commands'][cmd_name])
    except:
      self._push(Message(
        type=MsgType.Response, code=ResponseCode.CmdInvalid))
      return
    subs = copy.deepcopy(msg['subs_list'])
    try:
      try:
        remap_subs(subs, Conf.kConf['substitution_remaps'])
        substitute_cmd_args(cmd_struct, subs)
      except ClientWarningException as e:
        self._push(Message(type=MsgType.Text, body=str(e)+'\n'))
      except Exception as e:
        raise ClientErrorException(str(e)) from e
      self.__exec_cmd(cmd_struct['exec_list'])
    except ForceStopException:
      raise
    except ClientErrorException as e:
      self._push(Message(type=MsgType.Text,
        body=''.join(['Exception: ', '"', str(e), '"', ' in command "',
          cmd_name, '"\n'])))
      self._push(Message(type=MsgType.Response, code=ResponseCode.CmdFail))
      raise
    self._push(Message(type=MsgType.Response, code=ResponseCode.CmdDone))

  def __exec_send_file(self, path, new_name=None):
    f_name = os.path.basename(path)
    if new_name is None:
      new_name = f_name
    f_path = os.path.abspath(path)
    try:
      fd = open(f_path, mode='rb')
    except Exception as e:
      raise ClientErrorException('Unable to open file!') from e
    self._push(Message(
      type=MsgType.Text,
      body=' '.join(['Server sending file', f_name, 'of size',
        str(os.path.getsize(f_path)), 'bytes\n'])
    ))
    self._push(Message(
      type=MsgType.Raw, name=new_name,
      body=str(base64.b64encode(fd.read()), 'ascii')
    ))
    fd.close()

  def __exec_recv_file(self, path):
    f_name = os.path.basename(path)
    f_path = os.path.abspath(path)
    self.expecting_raw = True
    self._push(Message(type=MsgType.FileRequest, name=f_name))
    while not self.__stop_flag and self.expecting_raw:
      try:
        msg = self._pop(timeout=kQueueTimeout)
      except queue.Empty:
        continue
      if msg['type'] == MsgType.Raw:
        if msg['name'] == f_name:
          self.expecting_raw = False
          with open(f_path, mode='wb') as fd:
            easyio.write_data(base64.b64decode(bytes(msg['body'], 'ascii')), fd)
        else:
          self._push(Message(type=MsgType.Text,
            body='Wrong filename received; expected {0}'.format(f_name)))
      else:
        self._push(Message(type=MsgType.Text,
          body='Invalid message received; expected raw!'))
    if self.__stop_flag:
      raise ForceStopException

  def __exec_std_cmd(self, exec_entry):
    proc = subprocess.Popen(exec_entry['exec_arr'],
      stdout=subprocess.PIPE, stderr=subprocess.STDOUT,
      cwd=exec_entry['dir'])
    f_flags = fcntl.fcntl(proc.stdout, fcntl.F_GETFL)
    fcntl.fcntl(proc.stdout, fcntl.F_SETFL, f_flags | os.O_NONBLOCK)
    while True:
      if self.__stop_flag:
        proc.send_signal(signal.SIGTERM)
        raise ForceStopException
      try:
        proc.wait(timeout=kProcessUpdateInterval)
        break #break out of infinite loop once process returns
      except subprocess.TimeoutExpired:
        continue
      except:
        proc.send_signal(signal.SIGTERM)
      finally:
        output = proc.stdout.read()
        if (not output is None) and output != '':
          msg = Message(type=MsgType.Text, body=str(output, 'ascii'))
          if exec_entry['client_output'] == False:
            msg.update({'not_to_client': True})
          self._push(msg)
    return proc.returncode

  # Receives a pre-processed client_command struct and executes each exec,
  # printing output messages/data to the client
  def __exec_cmd(self, exec_list):
    for exec_entry in exec_list:
      try:
        if exec_entry['exec_arr'][0] == 'send_file':
          f_path = os.path.join(exec_entry['dir'], exec_entry['exec_arr'][1])
          try:
            new_name = exec_entry['exec_arr'][2]
          except:
            new_name = None
          self.__exec_send_file(f_path, new_name)
        elif exec_entry['exec_arr'][0] == 'recv_file':
          f_path = os.path.join(exec_entry['dir'], exec_entry['exec_arr'][1])
          self.__exec_recv_file(f_path)
        else:
          ret = self.__exec_std_cmd(exec_entry)
          if ret != 0:
            raise subprocess.CalledProcessError(
              cmd=' '.join(exec_entry['exec_arr']), returncode=ret)
      except ForceStopException:
        raise
      except Exception as e:
        if exec_entry['fatal_failure'] or self.__stop_flag:
          raise ClientErrorException(str(e)) from e

# Remaps the substitutions in subs_dict according to remaps_dict if possible.
def remap_subs(subs_dict, remaps_dict):
  for key in subs_dict.keys():
    if key in remaps_dict:
      if remaps_dict[key]['hashed']:
        remap_hash = easyhash.gen_raw_hash(
          bytes(subs_dict[key], 'ascii'), remaps_dict[key])
        remap_key = easyhash.stringify_hash(remap_hash)
      else:
        remap_key = subs_dict[key]
      try:
        subs_dict[key] = remaps_dict[key]['remap_table'][remap_key]
      except KeyError as e:
        if remaps_dict[key]['if_missing'] == 'self':
          subs_dict[key] = remap_key # pass the hash, not original, if hashed
        elif remaps_dict[key]['if_missing'] == 'default':
          subs_dict[key] = remaps_dict[key]['default']
        elif remaps_dict[key]['if_missing'] == 'error':
          raise ClientErrorException(
            'Illegal argument value "%s" for substitution list argument "%s"'
            %(subs_dict[key], key)
          ) from e
        else:
          raise RuntimeError('Extraneous value in config file in '
            '"substitution_remaps" under %s! %s is not a valid '
            'value for "if_missing"!' %(key, remaps_dict['if_missing'])
          )

# Finds "arg_name" in every argument of the exec_list and replaces it with "arg_sub"
def substitute_arg_in_list(exec_list, arg_name, arg_sub):
  for exec_entry in exec_list:
    for i, arg in enumerate(exec_entry['exec_arr']):
      exec_entry['exec_arr'][i] = re.sub(arg_name, arg_sub, arg)
    try:
      exec_entry['dir'] = re.sub(arg_name, arg_sub, exec_entry['dir'])
    except:
      pass

# Performs the argument substitutions with values in subs allowed by the
# cmd_struct. Fills in default values for any unsupplied arguments if possible.
def substitute_cmd_args(cmd_struct, subs):
  for arg_name in cmd_struct['substitution_list']:
    try:
      arg_sub = subs[arg_name]
      del subs[arg_name]
    except KeyError:
      try:
        arg_sub = cmd_struct['default_list'][arg_name]
      except KeyError as e:
        raise ClientErrorException('Required argument "%s" not provided'
          %arg_name) from e
    try:
      del cmd_struct['default_list'][arg_name]
    except:
      pass
    substitute_arg_in_list(cmd_struct['exec_list'], arg_name, arg_sub)

  for arg_name, arg_sub in cmd_struct['default_list'].items():
    substitute_arg_in_list(cmd_struct['exec_list'], arg_name, arg_sub)

  # if there are any remaining elements in subs, then they are unused and invalid
  if len(subs.keys()) > 0:
    raise ClientWarningException(
      'Invalid substitution list arguments %s ignored' %list(subs.keys()))
