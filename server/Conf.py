import socket, ssl
kConf = { #these are just the default settings
  'socket_type': 'inet',
  'socket_address': 9091,
  'log_file': None,
  'client_timeout': 30,
  'client_max_msg_len': 204800,
  'ssl_enabled': False,
  'ssl_server_cert': '',
  'ssl_server_key': '',
  'ssl_client_auth': 'none',
  'client_commands': {},
  'substitution_remaps': {}
}

kSocketTypeMap = {
  'unix': socket.AF_UNIX,
  'inet': socket.AF_INET,
  'inet6': socket.AF_INET6,
}

kSSLClientAuthMap = {
  'none': ssl.CERT_NONE,
  'optional': ssl.CERT_OPTIONAL,
  'required': ssl.CERT_REQUIRED,
}
