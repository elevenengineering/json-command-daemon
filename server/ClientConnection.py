import sys, random, io, traceback, queue, time
from common import Timer, SocketIO, Worker
from common.protocol import Message, MsgType, ResponseCode
from server import Conf, CommandExecutor

kIgnMsg = Message(type=MsgType.Response, code=ResponseCode.MsgIgnored)
kInvMsg = Message(type=MsgType.Response, code=ResponseCode.MsgInvalid)
kAckMsg = Message(type=MsgType.Ack)
kCloseMsg = Message(type=MsgType.CloseConnection)

kQueueTimeout = 0.01 #s

# Main function for every thread spawned to handle each individual client
# connection and make executive decisions
class ClientConnection(Worker.Worker):
  def __init__(self, socket, log_queue):
    super().__init__()
    self.__log_params = (socket.getpeername(), random.randint(0, sys.maxsize))
    self.__log_queue = log_queue
    self.__cmd_worker = CommandExecutor.CommandExecutor()
    self.__io = SocketIO.SocketIO(socket, Conf.kConf['client_max_msg_len'])
    self.__stop_flag = False
    self.__watchdog = Timer.Timer(Conf.kConf['client_timeout'])

  def run(self):
    self.__log_msg('Connection established with peer')
    self.__cmd_worker.start()
    self.__io.start()
    self.__watchdog.Reset()
    try:
      while not self.__stop_flag:
        if self.__watchdog.Elapsed():
          raise RuntimeError('Client timed out!')
        self.__service_cmd_worker()
        self.__service_io()
    except:
      s = io.StringIO()
      traceback.print_exc(file=s)
      self.__log_msg('\n'.join([s.getvalue(),
        'Unexpected disconnection to peer']))
      s.close()
      try:
        self.__send_msg(kCloseMsg)
      except:
        pass
    finally:
      self.__cmd_worker.stop()
      while self.__io.HasWork() and self.__io.is_alive():
        time.sleep(kQueueTimeout)
      self.__io.stop()
      self.__cmd_worker.join()
      self.__io.join()

  def stop(self):
    self.__stop_flag = True

  def __log_msg(self, contents, incoming=True):
    self.__log_queue.put((contents, self.__log_params[0], self.__log_params[1],
      incoming))

  def __send_msg(self, msg):
    self.__log_msg(msg, incoming=False)
    self.__io.put(msg)

  def __service_cmd_worker(self):
    if not self.__cmd_worker.is_alive():
      raise RuntimeError('Command worker thread unexpectedly died!')
    try:
      msg = self.__cmd_worker.get(timeout=kQueueTimeout)
    except queue.Empty:
      return
    if 'not_to_client' in msg:
      self.__log_msg(msg, incoming=False)
    else:
      self.__send_msg(msg)

  def __service_io(self):
    if not self.__io.is_alive():
      raise RuntimeError('IO thread unexpectedly died!')
    try:
      msg = self.__io.get(timeout=kQueueTimeout)
    except queue.Empty:
      return
    self.__log_msg(msg, incoming=True)
    if not msg.IsValid():
      self.__send_msg(kInvMsg)

    elif msg['type'] == MsgType.CloseConnection:
      self.__log_msg('Normal disconnection to peer')
      self.stop()

    elif msg['type'] == MsgType.Ack:
      if self.__cmd_worker.working and not self.__cmd_worker.expecting_raw:
        self.__watchdog.Reset()
      else:
        self.__send_msg(kIgnMsg)

    elif msg['type'] == MsgType.Raw:
      if self.__cmd_worker.expecting_raw:
        self.__cmd_worker.put(msg)
        self.__watchdog.Reset()
      else:
        self.__send_msg(kIgnMsg)

    elif msg['type'] == MsgType.Command:
      if self.__cmd_worker.working:
        self.__send_msg(kIgnMsg)
      else:
        self.__cmd_worker.put(msg)
        self.__send_msg(kAckMsg)
        self.__watchdog.Reset()

    else:
      self.__send_msg(kIgnMsg)
