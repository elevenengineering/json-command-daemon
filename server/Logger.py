import json, queue, sys, traceback, io, threading
from datetime import datetime
from copy import deepcopy
from common import protocol, Timer

kFlushTime = 10 #s
kQueueTimeout = 0.2 #s

# Thread which handles all logging
class Logger(threading.Thread):
  def __init__(self, arg_queue):
    super().__init__()
    self.arg_queue = arg_queue
    self.stop_flag = False

  def run(self):
    flush_timer = Timer.Timer(kFlushTime)
    flush_timer.Reset()
    while not self.stop_flag:
      if flush_timer.Elapsed():
        sys.stdout.flush()
        sys.stderr.flush()
        flush_timer.Reset()
      try:
        self.__pop_and_log()
      except queue.Empty:
        continue
      except:
        s = io.StringIO()
        traceback.print_exc(file=s)
        log_str(s.getvalue())
        s.close()
    while True:
      try:
        self.__pop_and_log() # clean out whatever messages may be lingering
      except:
        break
    sys.stdout.flush()
    sys.stderr.flush()

  def stop(self):
    self.stop_flag = True

  def __pop_and_log(self):
    args = self.arg_queue.get(timeout=kQueueTimeout)
    if isinstance(args[0], str):
      log_str(*args)
    else:
      log_msg(*args)

def log_str(s, peer='unknown', sess_id='unknown', *_):
  print(' '.join([ s, common_preamble(peer, sess_id) ]))

# Writes msg to the log, formatted with metadata.
# The contents of the JSON message will be printed "prettily".
# incoming determines whether the message is incoming or not
def log_msg(msg, peer='unknown', sess_id='unknown', incoming=True):
  if not isinstance(msg, protocol.Message):
    raise RuntimeWarning('Invalid argument type provided to log_msg!')
  preamble = ''
  if incoming:
    preamble = 'Received message from'
  else:
    preamble = 'Sent message to'
    if 'not_to_client' in msg:
      del msg['not_to_client']
      preamble = 'Did not send message to'
  print(' '.join([ preamble, common_preamble(peer, sess_id) ]))
  if msg['type'] == protocol.MsgType.Raw:
    mlocal = protocol.Message(type=protocol.MsgType.Raw.name, body='')
  else:
    mlocal = deepcopy(msg) #this is nasty :(
    mlocal['type'] = protocol.MsgType(mlocal['type']).name
    try:
      mlocal['code'] = protocol.ResponseCode(mlocal['code']).name
    except:
      pass
  json.dump(mlocal, sys.stdout, sort_keys=True, indent=2)
  print('')

def common_preamble(peer, sess_id):
  return '{0} at {1} with session id {2}'.format(peer, datetime.now(), sess_id)
