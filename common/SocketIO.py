import socket, queue, ssl
from common import protocol, Worker

kMaxSocketBufSize = 4096 #bytes #FIXME: make this configurable?
kSocketTimeout = 0.01 #s
kQueueTimeout = kSocketTimeout

class SocketIO(Worker.Producer, Worker.Consumer):
  def __init__(self, socket, max_len):
    super().__init__()
    self.__socket = socket
    self.__max_len = max_len
    self.__curr_msg = bytearray()
    self.__curr_ind = 0
    self.__socket.settimeout(kSocketTimeout)
    self.__buf = protocol.RawMsgBuffer()
    self.__stop_flag = False

  def run(self):
    try:
      while not self.__stop_flag:
        self.__send_data()
        self.__recv_data()
    except:
      pass
    finally:
      try:
        self.__socket.shutdown(socket.SHUT_RDWR)
        self.__socket.close()
      except:
        pass

  def stop(self):
    self.__stop_flag = True

  def __send_data(self):
    if len(self.__curr_msg) == 0:
      try:
        self.__curr_msg.extend(self._pop(timeout=kQueueTimeout).ToRaw())
      except queue.Empty:
        return
      self.__curr_msg.extend(protocol.kMsgDelimiter)
      self.__curr_ind = 0
    if self.__curr_ind < len(self.__curr_msg):
      try:
        self.__curr_ind += self.__socket.send(
          memoryview(self.__curr_msg[self.__curr_ind:]).tobytes())
      except (socket.timeout, ssl.SSLWantReadError):
        return
    if self.__curr_ind == len(self.__curr_msg):
      self.__curr_msg.clear()

  def __recv_data(self):
    try:
      self.__buf.extend(self.__socket.recv(kMaxSocketBufSize))
      if len(self.__buf) > self.__max_len:
        raise RuntimeError('Anomalously long request from peer!')
    except (socket.timeout, ssl.SSLWantWriteError):
      pass
    msg = self.__buf.extract_msg()
    if not msg is None:
      self._push(msg)
